
public class StudentInfo {
	// To display Studentinfo using class and objects
	int rollno;
	String name;
	int mark1,mark2,mark3;
	
	void displayInfo(int r,String n)
	{
		System.out.println("Student RollNo:"+r);
		System.out.println("Student Name:"+n);
	}
	
	int calculateTotal(int m1,int m2, int m3)
	{
		int total=m1+m2+m3;
		return total;
	}
	
	float calAverage(float avg)
	{
		return (avg/3);
	}
	
	
	
	public static void main(String[] args) {
		
		StudentInfo stu = new StudentInfo();
		
		stu.displayInfo(11, "AAAA");
		int total = stu.calculateTotal(97,98,99);
		System.out.println("Total Marks:"+total);
		System.out.println("Average:"+stu.calAverage(total));
		System.out.println("-----------------------------");
		
		stu.displayInfo(12, "BBBB");
		int total1 = stu.calculateTotal(93,90,60);
		System.out.println("Total Marks:"+total1);
		System.out.println("Average:"+stu.calAverage(total1));
		System.out.println("-----------------------------");
		
		stu.displayInfo(12, "CCCC");
		int total2 = stu.calculateTotal(81,67,60);
		System.out.println("Total Marks:"+total2);
		System.out.println("Average:"+stu.calAverage(total2));
		System.out.println("-----------------------------");
		
	}

}
