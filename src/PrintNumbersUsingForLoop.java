
public class PrintNumbersUsingForLoop {

	public static void main(String[] args) {
		// Program to Print 10 to 1 numbers using for loop
		
		int i;
		System.out.println("Print Numbers 10 to 1");
		
		for(i=10;i>=1;i--)
		{
			System.out.println(i);
		}

	}

}
