import java.util.Scanner;

public class PalindromeOrNot {

	public static void main(String[] args) {
		// Program to check if given number is palindrome or not
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the number");
		int a = sc.nextInt();
		int c = 0;
		int b, d = a;
		while(a != 0) {
		    
		      b = a % 10;//Eg:12321 is given number then b=12321%10; b=1
		      c = c * 10 + b;// c=0*10+1; c=1
		      a /= 10;//a=12321/10; a=1232 ; Repeat the same until a!=0
		    }
		if(c==d)
		{

		    System.out.println("The given number is a Palindrome");
		}
		else
		{
			System.out.println("The given number is not a Palindrome");
		}
		

	}

}
