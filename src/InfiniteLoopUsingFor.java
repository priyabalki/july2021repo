
public class InfiniteLoopUsingFor {

	public static void main(String[] args) {
		// Infinite loop using for

		int i;
		System.out.println("Infinite Loop Using For Loop");
		
		for(i=1;i>0;i++)
		{
			System.out.println(i);
		}
	}

}
