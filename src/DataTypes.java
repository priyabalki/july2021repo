
public class DataTypes {

	public static void main(String[] args) {
		//Java program to declare two integer variables, one float variable, and one string variable
		// and assign values to them. Then display their values on the screen.
		
		int a = 10, b = 20;
		float c = 12.5f;
		String s = "JAVA PROGRAMMING";
		System.out.println("The value of an integer a = "+a);
		System.out.println("The value of an integer b = "+b);
		System.out.println("The float value of c = "+c);
		System.out.println("The String s = "+s);
		

	}

}
