import java.util.Scanner;

public class FindTheGreatestNumber {

	public static void main(String[] args) {
		// Program to find greatest of three numbers
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the number for a = ");
		int a = sc.nextInt();
		System.out.println("Enter the number for b = ");
		int b = sc.nextInt();
		System.out.println("Enter the number for c = ");
		int c = sc.nextInt();
		
		if(a>b && a>c)
		{
			System.out.println("A is greater");
		}
		else if(b>c)
		{
			System.out.println("B is greater");
		}
		else
		{
			System.out.println("C is greater");
		}
		
		
	}

}
