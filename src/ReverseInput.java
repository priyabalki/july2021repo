import java.util.Scanner;

public class ReverseInput {

	public static void main(String[] args) {
		// Reverse an input number 
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the number");
		int a = sc.nextInt();
		int c = 0;
		int b;
		while(a != 0) {
		    
		      b = a % 10;//Eg:1234 is given number then b=1234%10; b=4
		      c = c * 10 + b;// c=0*10+4; c=4
		      a /= 10;//a=1234/10; a=123 ; Repeat the same until a!=0
		    }

		    System.out.println("Reversed Number: " + c);
		

	}

}
