
public class PrintNumbersUsingWhile {

	public static void main(String[] args) {
		// Program to Print 1 to 10 numbers using while loop
		
		int a = 1;
		System.out.println("Print Numbers 1 to 10:");
		
		while (a<=10)
		{
			System.out.println(a);
			a++;
		}

	}

}
