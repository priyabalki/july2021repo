import java.util.Scanner;

public class StudentsGrade {

	public static void main(String[] args) {
		/*The program to determine the grade based on the following rules:
		        -if the average score >=90% =>grade=A
				-if the average score >= 70% and <90% => grade=B
				-if the average score>=50% and <70% =>grade=C
				-if the average score<50% =>grade=F */
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the first score = ");
		double score1 = sc.nextInt();
		System.out.println("Enter the second score = ");
		double score2 = sc.nextInt();
		System.out.println("Enter the third score = ");
		double score3 = sc.nextInt();
		System.out.println("Enter the Fourth score = ");
		double score4  = sc.nextInt();
		System.out.println("Enter the fifth score = ");
		double score5  = sc.nextInt();
		double average = (score1+score2+score3+score4+score5)/5;
		
		if(average >= 90)
		{
			System.out.println("You are in Grade A");
		}
		else if(average >=70 && average <90)
		{
			System.out.println("You are in Grade B");
		}
		else if(average >= 50 && average < 70)
		{
			System.out.println("You are in Grade C");
		}
		else
		{
			System.out.println("You are in Grade F");
		}

	}

}
