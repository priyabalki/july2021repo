
public class ContinueLoopUsingForAndWhileLoop {

	public static void main(String[] args) {
		// Continue the loop using if condition in for/while loop
		
				// Continue the loop using if condition in for loop
				
				int i=0,j=0;
				
				System.out.println("Continue the loop using if condition in for loop");
				
				for(i=0;i<=10;i++)
				{
					
					if(i==8)
					{
						continue;
					}
					
					System.out.println("The value of i "+i);
				}
				
				System.out.println("Exit For Loop");
				
				// Continue the loop using if condition in while loop
				
				System.out.println("Continue the loop using if condition in while loop");	
				while(j<10)
				{
					
					j++;
					
					if(j==3)
					{
						continue;
					
					}
					
					System.out.println("The Value of j "+j);
					
				}
				System.out.println("Exit While Loop");

			}

	}

