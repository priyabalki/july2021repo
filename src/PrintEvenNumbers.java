
public class PrintEvenNumbers {

	public static void main(String[] args) {
		// Print even numbers from 2 to 20
		
		int i;
		
		System.out.println("Print even numbers from 2 to 20");
		
		for(i=2;i<=20;i=i+2)
		{
			System.out.println(i);
		}

	}

}
