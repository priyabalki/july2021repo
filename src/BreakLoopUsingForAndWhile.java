
public class BreakLoopUsingForAndWhile {

	public static void main(String[] args) {
		// Break the loop using if condition in for/while loop
		
		// Break the loop using if condition in for loop
		
		int i=0,j=0;
		
		System.out.println("Break the loop using if condition in for loop");
		
		for(i=0;i<=10;i++)
		{
			System.out.println("The value of i "+i);
			if(i==8)
			{
				break;
			}
		}
		System.out.println("Exit For Loop");
		
		// Break the loop using if condition in while loop
		
		System.out.println("Break the loop using if condition in while loop");	
		while(j>=0)
		{
			System.out.println("The Value of j "+j);
			if(j==3)
			{
				break;
			}
			j++;
		}
		System.out.println("Exit While Loop");

	}

}
