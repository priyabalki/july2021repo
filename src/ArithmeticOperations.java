
public class ArithmeticOperations {

	public static void main(String[] args) {
		/*Java program to allow the user to input two integer values and 
		 * then the program prints the results of adding, subtracting, multiplying, 
		 * and dividing among the two values.
		 */
		int a = 30,b = 10;
		System.out.println("The result of addition is "+(a+b));
		System.out.println("The result of subtraction is "+(a-b));
		System.out.println("The result of multiplication is "+(a*b));
		System.out.println("The result of division is "+(a/b));
	

	}

}
