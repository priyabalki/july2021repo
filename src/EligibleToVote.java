import java.util.Scanner;

public class EligibleToVote {

	
	public static void main(String[] args) {
		/*Java program to allow the user to input his/her age. 
		 * Then the program will show if the person is eligible to vote. 
		 * A person who is eligible to vote must be older than or equal to 18 years old.
		 */

		int n;
        Scanner s = new Scanner(System.in);
        System.out.print("Enter your age: ");
        n = s.nextInt();
        if (n>=18)
        {
        	System.out.println("You are eligible to vote");
        }
        else
        {
        	System.out.println("You are not eligible to vote");
        }
        }

	}


