
public class InfiniteLoopUsingWhile {

	public static void main(String[] args) {
		// Infinite loop using While
		
		int i=1;
		System.out.println("Infinite Loop using While");
		
		while(i>0)
		{
			System.out.println(i);
			i++;
		}

	}

}
