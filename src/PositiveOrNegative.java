import java.util.Scanner;

public class PositiveOrNegative {

	public static void main(String[] args) {
		// Program to check whether the given number is positive or negative

		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter your number:");
		int a = sc.nextInt();
		
		if(a > 0)
		{
			System.out.println("The given number is positive");
		}
		else
		{
			System.out.println("The given number is negative");
		}
		
		

	}

}
