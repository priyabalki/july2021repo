
public class OddOrEven {

	public static void main(String[] args) {
		// Program to check if number is odd or even
		int a = 22;
		if((a % 2) == 0)
		{ 
			System.out.println("The number is even");
		}
		else 
		{
			System.out.println("The number is odd");
		}
		
	}

}
